<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('employee','App\Http\Controllers\EmployeeController@index')->name('employee');
Route::post('employee/save','App\Http\Controllers\EmployeeController@saveRecord')->name('employee/save');
