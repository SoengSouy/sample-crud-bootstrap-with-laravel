<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Models\Employees;

class EmployeeController extends Controller
{
    /** index page */
    public function index()
    {
        $EmployeeList = Employees::all();
        return view('employee',compact('EmployeeList'));
    }

    /** save record */
    public function saveRecord(Request $request)
    {
        DB::beginTransaction();
        try {
            $EmployeeList          = new Employees;
            $EmployeeList->name    = $request->name;
            $EmployeeList->email   = $request->email;
            $EmployeeList->phone   = $request->phone;
            $EmployeeList->address = $request->address;
            $EmployeeList->save();
            DB::commit();
            return redirect()->back();
        } catch(\Exception $e) {
            DB::rollback();
            \Log::info($e);
            return redirect()->back();
        }
    }
}
